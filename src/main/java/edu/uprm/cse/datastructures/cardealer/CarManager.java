package edu.uprm.cse.datastructures.cardealer;


import java.util.ArrayList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructure.carservice.JsonError;
import edu.uprm.cse.datastructure.carservice.NotFoundException;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	
	private static HashTableOA<Long, Car> cTable = CarTable.getInstance();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] carArray = new Car[cTable.size()];
		if (cTable.isEmpty()) {
			return carArray;
		}

		for (int i = 0; i < cTable.size(); i++) { 
			carArray[i] = cTable.getValues().get(i);
		}
		return carArray;

	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		if (cTable.get(id) != null) {
			return cTable.get(id);
		}
		throw new NotFoundException();
	

	} 
	
	@POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){	
		cTable = CarTable.getInstance();
		if (!cTable.contains(car.getCarId())) {
			cTable.put(car.getCarId(), car);
			return Response.status(201).build();
		}
		return Response.status(404).build();

	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car updCar) {
		if (cTable.contains(updCar.getCarId())) {
			cTable.put(updCar.getCarId(), updCar);
			return Response.status(Response.Status.OK).build();
		}
		return Response.status(404).build();

    }
	
	@DELETE
    @Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		if (cTable.contains(id)) {
			cTable.remove(id);
			return Response.status(Response.Status.OK).build();
		}
		return Response.status(404).build();
	}
}

