package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
	//creates the private carList to use in the API Test
		private static HashTableOA<Long, Car> carTable = new HashTableOA<Long, Car>(new CarComparator(), new LongComparator());
		
		private CarTable() {}
		
		public static HashTableOA<Long, Car> getInstance() {
			return carTable;
		}
		
		public static void resetCars() {
			carTable.clear();
		}
}
