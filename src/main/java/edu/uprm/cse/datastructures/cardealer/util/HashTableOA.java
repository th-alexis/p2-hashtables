package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

public class HashTableOA<K, V> implements Map<K, V> {
	
	//creates the keys and values that every element should have
	//inside the buckets
	private static class MapEntry<K, V> {
		private K key;
		private V value;
		
		
		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}

		public K getKey() {
			return key;
		}

		public void setKey(K key) {
			this.key = key;
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}

	}
	
	//the constructor of the Bucket, the bucket has the elements that
	//is a MapEntry and if the Bucket is in use or not
	private class Bucket{
		private MapEntry<K, V> element;
		private boolean occupied = false;
		
		public Bucket(MapEntry<K, V> element, boolean occupied) {
			super();
			this.element = new MapEntry<K, V>(null, null);
			this.occupied = occupied;
			
		}

		public MapEntry<K, V> getElement() {
			return element;
		}

		public void setElement(MapEntry<K, V> element) {
			this.element = element;
		}

		public boolean isOccupied() {
			return occupied;
		}

		public void setOccupied(boolean occupied) {
			this.occupied = occupied;
		}
		
	}
	
	private int currentSize;
	private Object[] buckets;
	//comparator for the values
	private Comparator<V> ccmp;
	//comparator for the keys
	private Comparator<K> lcmp;
	private static final int INITIAL_CAPACITY = 11;
	
	public HashTableOA(int INITIAL_CAPACITY, Comparator<V> ccmp, Comparator<K> lcmp) {
		this.ccmp = ccmp;
		this.lcmp = lcmp;
		this.currentSize = 0;
		//creating the list of objects with buckets
		this.buckets = new Object[INITIAL_CAPACITY];
		//filling up the list of objects with buckets
		for(int i=0; i < INITIAL_CAPACITY; ++i) {
			this.buckets[i] = new Bucket(null, false);
		}
	}
	
	//second constructor of HashTable that only passes the comparators
	public HashTableOA(Comparator<V> ccmp, Comparator<K> lcmp) {
		this(INITIAL_CAPACITY, ccmp, lcmp);
	}
	
	//first hashing
	private int hashFunction(K key) {
		return key.hashCode() % this.buckets.length;
	}
	
	//second hashing
	private int doubleHashFucntion(K key) {
		return 3 - hashFunction(key) % 3;
	}
	
	//linear probing that gets a integer and loops throw the list of buckets
	//and then gets the key of the space in the list that is not occupied
	private int linearProbingFunction(int index) {
		for(int j = (index + 1) % this.buckets.length; j != index; j = (j + 1) % this.buckets.length) {
			Bucket B = (Bucket) this.buckets[j];			
			if(!B.occupied)
				return j;
		}
		
		return -1;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		//checks if the key if null, if it is throws a exception
		if(key == null) 
			throw new IllegalArgumentException();
		
		//prepares the first hash in which the bucket may appear
		int index = hashFunction(key);
		Bucket tempBucket = (Bucket) this.buckets[index];
		
		//checks if the bucket is occupied and if the key is equal to the one
		//given if it is it will return the value
		if(tempBucket.occupied && lcmp.compare(key, tempBucket.getElement().getKey()) == 0)
			return tempBucket.getElement().getValue();
		else {
			//prepares the double hash in which the bucket may appear if it
			//does not appear with the first hash
			int doubleIndex = doubleHashFucntion(key);
			Bucket tempBucket2 = (Bucket) this.buckets[doubleIndex];
			
			//checks if the bucket is occupied and if the key is equal to the one
			//given if it is it will return the value
			if(tempBucket2.occupied && lcmp.compare(key, tempBucket2.getElement().getKey()) == 0)
				return tempBucket2.getElement().getValue();
			else {
				//prepares the linear hash in which the bucket may appear if it
				//does not appear with the double hash
				int linearIndex = linearProbingFunction(doubleIndex);
				Bucket tempBucket3 = (Bucket) this.buckets[linearIndex];
				
				//checks if the bucket is occupied and if the key is equal to the one
				//given if it is it will return the value
				if(tempBucket3.occupied && lcmp.compare(key, tempBucket3.getElement().getKey()) == 0)
					return tempBucket3.getElement().getValue();
				
				//it will return null because it did not found the key never
				else
					return null;
				
			}
		}
	}

	@Override
	public V put(K key, V value) {
		//first will check if all the buckets are full and if they
		//are reallocate and then double the size of the list of buckets
		if(this.buckets.length == this.currentSize)
			this.reallocate(key, value);
		
		//saves the old value so we can return it later when we find the
		//space where the bucket will go
		V oldValue = this.get(key);
		
		//checks if the bucket that we are trying to insert is already in the
		//so we can update it or if is not on the list just insert it
		if(oldValue != null) {

			//prepares the first hash in which the bucket may appear
			int index = hashFunction(key);
			Bucket tempBucket = (Bucket) this.buckets[index];
			
			//checks if the bucket is occupied and if the keys are equal to update
			//the value
			if(tempBucket.occupied && tempBucket.getElement().getKey().equals(key)) {
				tempBucket.getElement().setValue(value);
				return oldValue; 
			}else {
				//prepares the double hash in which the bucket may appear if it
				//does not appear with the first hash
				int doubleIndex = doubleHashFucntion(key);
				Bucket tempBucket2 = (Bucket) this.buckets[doubleIndex];
				
				////checks if the bucket is occupied and if the keys are equal to update
				//the value
				if(tempBucket2.occupied && tempBucket2.getElement().getKey().equals(key)) {
					tempBucket2.getElement().setValue(value);
					return oldValue;
				}else {
					//prepares the linear hash in which the bucket may appear if it
					//does not appear with the double hash
					int linearIndex = linearProbingFunction(doubleIndex);
					Bucket tempBucket3 = (Bucket) this.buckets[linearIndex];
					
					//////checks if the bucket is occupied and if the keys are equal to update
					//the value
					tempBucket3.getElement().setValue(value);
					return oldValue;
					
				}
			}
		//if we have to put the object in a new bucket the we do this process	
		}else {
			//prepares the first hash in which the bucket may appear
			int index = hashFunction(key);
			Bucket tempBucket = (Bucket) this.buckets[index];
			
			//if the bucket is not occupied then it will proceed to insert the new bucket
			//and return null because there was not a old value
			if(!tempBucket.occupied) {
				tempBucket.getElement().setKey(key);
				tempBucket.getElement().setValue(value);
				tempBucket.occupied = true;
				this.currentSize++;
				return null; 
			}else {
				//prepares the double hash in which the bucket may appear if it
				//does not appear with the first hash
				int doubleIndex = doubleHashFucntion(key);
				Bucket tempBucket2 = (Bucket) this.buckets[doubleIndex];

				////if the bucket is not occupied then it will proceed to insert the new bucket
				//and return null because there was not a old value
				if(!tempBucket2.occupied) {
					tempBucket2.getElement().setKey(key);
					tempBucket2.getElement().setValue(value);
					tempBucket2.occupied = true;
					this.currentSize++;
					return null;
				}else {
					//prepares the linear hash in which the bucket may appear if it
					//does not appear with the double hash
					int linearIndex = linearProbingFunction(doubleIndex);
					Bucket tempBucket3 = (Bucket) this.buckets[linearIndex];
					
					//inserts the new bucket and return null because there was not a old value
					tempBucket3.getElement().setKey(key);
					tempBucket3.getElement().setValue(value);
					tempBucket3.occupied = true;
					this.currentSize++;
					return null;
					
				}
			}
		}

	}
	
	//a function to reallocate the buckets in a full list of buckets
	private void reallocate(K key, V value) {
		Object[] tempBucket = this.buckets;
		//creates the array with double the length
		this.buckets = new Object[this.buckets.length * 2];
		this.currentSize = 0;
		
		//fills the list with buckets
		for(int i=0; i<this.buckets.length; ++i) {
			this.buckets[i] = new Bucket(null, false);
		}
		
		//then it allocates the old buckets on the new list
		for(int i=0; i<tempBucket.length; ++i) {
			Bucket tempBucket2 = (Bucket) tempBucket[i];
			this.put(tempBucket2.getElement().getKey(), tempBucket2.getElement().getValue());
		}
		
		//then will put the new value in the new list
		this.put(key, value);
	}

	@Override
	public V remove(K key) {
		//checks if the key if null, if it is throws a exception
		if(key == null)
			throw new IllegalArgumentException();
		
		//prepares the first hash in which the bucket may appear
		int index = hashFunction(key);
		Bucket tempBucket = (Bucket) this.buckets[index];
		
		//a checker that will be updated will the value if it finds it
		//if not it will stay null, basically to only return the value
		V checker = null;
		
		//checks if the but is occupied, because if it is not occupied there nothing to remove
		//and checks if the key are equal
		if(tempBucket.occupied && lcmp.compare(key, tempBucket.getElement().getKey()) == 0) {
			checker = tempBucket.getElement().getValue();
			this.buckets[index] = new Bucket(null, false);
			this.currentSize--;

		}else {
			//prepares the double hash in which the bucket may appear if it
			//does not appear with the first hash
			int doubleIndex = doubleHashFucntion(key);
			tempBucket = (Bucket) this.buckets[doubleIndex];
			
			//checks if the but is occupied, because if it is not occupied there nothing to remove
			//and checks if the key are equal
			if(tempBucket.occupied && lcmp.compare(key, tempBucket.getElement().getKey()) == 0) {
				checker = tempBucket.getElement().getValue();
				this.buckets[doubleIndex] = new Bucket(null, false);
				this.currentSize--;
			}else {
				//prepares the linear hash in which the bucket may appear if it
				//does not appear with the double hash
				int linearIndex = linearProbingFunction(doubleIndex);
				tempBucket = (Bucket) this.buckets[linearIndex];
				
				//checks if the but is occupied, because if it is not occupied there nothing to remove
				//and checks if the key are equal
				if(tempBucket.occupied && lcmp.compare(key, tempBucket.getElement().getKey()) == 0) {
					checker = tempBucket.getElement().getValue();
					this.buckets[linearIndex] = new Bucket(null, false);
					this.currentSize--;
				}
			}
		}
		return checker;
		
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}

	@Override
	public SortedList<K> getKeys() {
		//creates the list in which the key will be stored
		SortedList<K> result = new CircularSortedDoublyLinkedList<K>(lcmp);
		Bucket tempBucket = new Bucket(null, false);
		
		//goes for every item on the list of buckets
		for (int i = 0; i < buckets.length; i++) {
			tempBucket = (Bucket) buckets[i];
			//if it finds a occupied space it will get the key of the bucket
			if(tempBucket.occupied)
				result.add(tempBucket.getElement().getKey());
		}
		return result;

	}

	@Override
	public SortedList<V> getValues() {
		//creates the list in which the values will be stored
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>(ccmp);
		Bucket tempBucket = new Bucket(null, false);
		
		//goes for every item on the list of buckets
		for (int i = 0; i < buckets.length; i++) {
			tempBucket = (Bucket) buckets[i];
			//if it finds a occupied space it will get the key of the bucket
			if(tempBucket.occupied)
				result.add(tempBucket.getElement().getValue());
		}
		return result;
	}

	public void clear() {
		//goes foe very item in the list and the insert a brand new bucket
		//and at the same item deleting any old data
		for(int i=0; i<this.buckets.length; ++i) {
			this.buckets[i] = new Bucket(null, false);
		}
		this.currentSize = 0;
		
	}

}
